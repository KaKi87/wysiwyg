import { createEditor } from './src/editor.js';
import resetStyleSheet from './src/reset.css' assert { type: 'css' };

document.adoptedStyleSheets = [...document.adoptedStyleSheets || [], resetStyleSheet];

export { createEditor };