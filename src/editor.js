import { createBlock } from './block.js';
import editorStyleSheet from './editor.css' assert { type: 'css' };

document.adoptedStyleSheets = [...document.adoptedStyleSheets || [], editorStyleSheet];

export const createEditor = () => {
    const editorElement = document.createElement('ul');
    editorElement.classList.add('Editor');

    const blocks = [createBlock()];
    blocks[0].mount({ editorElement });

    return {
        /** @param {HTMLElement} containerElement */
        mount: ({ containerElement }) => {
            containerElement.appendChild(editorElement);
            blocks[0].focus({ inlineIndex: 0 });
        },
        /** @param {string} modifier */
        modifySelection: ({ modifier }) => {
            const {
                blockIndex,
                inlineIndex,
                start,
                end
            } = blocks
                .map((block, index) => ({ ...block.getSelection(), blockIndex: index }))
                .find(({ start, end }) => typeof start === 'number' && typeof end === 'number');
            blocks[blockIndex].split({
                inlineIndex,
                start,
                end,
                modifier
            });
        }
    };
};