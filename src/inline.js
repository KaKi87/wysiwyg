import inlineStyleSheet from './inline.css' assert { type: 'css' };

document.adoptedStyleSheets = [...document.adoptedStyleSheets || [], inlineStyleSheet];

export const createInline = () => {
    const inlineElement = document.createElement('li');
    inlineElement.classList.add('Editor__Block__InlineContainer__Inline');

    const inputElement = document.createElement('input');
    inputElement.classList.add('Editor__Block__InlineContainer__Inline__Input');
    inlineElement.appendChild(inputElement);

    const outputElement = document.createElement('span');
    outputElement.classList.add('Editor__Block__InlineContainer__Inline__Output');
    inlineElement.appendChild(outputElement);

    const onInput = () => { outputElement.innerText = inputElement.value };
    inputElement.addEventListener('input', onInput);

    return {
        /**
         * @param {HTMLElement} inlineContainerElement
         * @param {InsertPosition|number} position
         */
        mount: ({ inlineContainerElement, position }) => {
            if(typeof position === 'string')
                inlineContainerElement.insertAdjacentElement(position, inlineElement);
            else if(typeof position === 'number')
                inlineContainerElement.insertBefore(inlineElement, inlineContainerElement.children[position]);
        },
        focus: () => inputElement.focus(),
        /** @returns {{start: number, end: number}} */
        getSelection: () => {
            const
                {
                    selectionStart,
                    selectionEnd
                } = inputElement,
                isSelected = selectionStart !== selectionEnd;
            return {
                start: isSelected ? selectionStart : undefined,
                end: isSelected ? selectionEnd : undefined
            };
        },
        /** @returns {string} */
        getValue: () => inputElement.value,
        /** @param {string} value */
        setValue: ({ value }) => {
            inputElement.value = value;
            onInput();
        },
        /** @returns {string[]} */
        getModifiers: () => [...inlineElement.classList].map(className => className.split('--')[1]).filter(Boolean),
        /** @param {string} modifier */
        toggleModifier: ({ modifier }) => {
            inlineElement.classList.toggle(`Editor__Block__InlineContainer__Inline--${modifier}`);
        }
    };
};