import { createInline } from './inline.js';
import blockStyleSheet from './block.css' assert { type: 'css' };

document.adoptedStyleSheets = [...document.adoptedStyleSheets || [], blockStyleSheet];

export const createBlock = () => {
    const blockElement = document.createElement('li');
    blockElement.classList.add('Editor__Block');

    const inlineContainerElement = document.createElement('ul');
    inlineContainerElement.classList.add('Editor__Block__InlineContainer');
    blockElement.appendChild(inlineContainerElement);

    const inlines = [createInline()];
    inlines[0].mount({ inlineContainerElement, position: 'afterbegin' });

    return {
        /** @param {HTMLElement} editorElement */
        mount: ({ editorElement }) => {
            editorElement.appendChild(blockElement)
        },
        /** @param {number} inlineIndex */
        focus: ({ inlineIndex }) => inlines[inlineIndex].focus(),
        /** @returns {{start: number, end: number, inlineIndex: number}} */
        getSelection: () => inlines
            .map((inline, index) => ({ ...inline.getSelection(), inlineIndex: index }))
            .find(({ start, end }) => typeof start === 'number' && typeof end === 'number'),
        /**
         * @param {number} inlineIndex
         * @param {number} start
         * @param {number} end
         * @param {string} modifier
         */
        split: ({
            inlineIndex,
            start,
            end,
            modifier
        }) => {
            const value = inlines[inlineIndex].getValue();
            inlines[inlineIndex].setValue({ value: value.substring(0, start) });
            inlines.splice(inlineIndex + 1, 0, createInline());
            inlines[inlineIndex + 1].setValue({ value: value.substring(start, end) });
            inlines[inlineIndex].getModifiers().forEach(modifier => inlines[inlineIndex + 1].toggleModifier({ modifier }));
            inlines[inlineIndex + 1].toggleModifier({ modifier });
            inlines[inlineIndex + 1].mount({ inlineContainerElement, position: inlineIndex + 1 });
            inlines.splice(inlineIndex + 2, 0, createInline());
            inlines[inlineIndex + 2].setValue({ value: value.substring(end, value.length) });
            inlines[inlineIndex].getModifiers().forEach(modifier => inlines[inlineIndex + 2].toggleModifier({ modifier }));
            inlines[inlineIndex + 2].mount({ inlineContainerElement, position: inlineIndex + 2 });
        }
    };
};